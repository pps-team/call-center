# call-center

Call-center project repo for [Software Design Course](pps-design.org) at MIPT.

## Our team:
* Andrew Stepanov
* Alex Sofienko
* Ivan Butrim
* Nastya Torunova

## Development environment setup

* You should have `docker` and `docker-compose`

*This will save you from setting up Postgres with Django locally.*
* Run `docker-compose build`
* Run `docker-compose run --rm webapp ./manage.py migrate`

*You can run any command in webapp container just by executing `docker-compose run --rm webapp command` in your shell.*
* Run `docker-compose up`

*If you pass `-d` to this command it will run as a daemon and won't output anything to terminal.*
