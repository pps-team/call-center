FROM python:3.5.1
RUN apt-get update && apt-get install -y wget
ENV DOCKERIZE_VERSION v0.2.0
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz
ENV PYTHONUNBUFFERED 1
RUN mkdir /project_code
WORKDIR /project_code
COPY requirements.txt /project_code/
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
COPY ./call-center-django /project_code/
