from django.db import models
from call_center.models import ProblemCategory, ServiceRequest
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
# Create your models here.


class Operator(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    categories = models.ManyToManyField(ProblemCategory)

    def get_service_requests(self):
        requests = (ServiceRequest.objects.filter(category__in=self.categories.all(),
                                                  status__in=[ServiceRequest.CREATED,
                                                              ServiceRequest.REDIRECTED])
                                          .exclude(operator_id=self.user).order_by('status')[:10])
        return requests

    def __str__(self):
        return "Name: {} categories: {}".format(self.user.username,
                                                (" "
                                                 .join(map(lambda x: x.name,
                                                           self.categories.all()))))


@receiver(post_save, sender=User)
def create_operator(sender, instance, **kwargs):
    operator, created = Operator.objects.get_or_create(user=instance)
