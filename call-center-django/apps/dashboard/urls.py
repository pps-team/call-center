from django.conf.urls import url, include
from .views import get_service_requests, accept_call, redirect_call, finish_call
app_name = 'dashboard'

urlpatterns = [
    url(r'accept/(?P<pk>\d+)$', accept_call, name="accept"),
    url(r'redirect/(?P<pk>\d+)$', redirect_call, name="redirect"),
    url(r'finish/(?P<pk>\d+)$', finish_call, name="finish"),
    url(r'$', get_service_requests, name="index"),
]
