from django.shortcuts import render, reverse, redirect
from .models import Operator
from call_center.models import Call, ServiceRequest
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
# Create your views here.


@login_required
def get_service_requests(request):
    context = {'requests': Operator.objects.get(user=request.user).get_service_requests()}
    return render(request, "dashboard/dashboard.html", context=context)


@login_required
@require_POST
def accept_call(request, pk):
    ServiceRequest.objects.get(call_id__pk=pk).accept_by(request.user)
    return redirect('call_center:join', pk=pk)


@login_required
@require_POST
def redirect_call(request, pk):
    ServiceRequest.objects.get(call_id__pk=pk).change_status(ServiceRequest.REDIRECTED)
    return redirect('dashboard:index')


@login_required
@require_POST
def finish_call(request, pk):
    ServiceRequest.objects.get(call_id__pk=pk).change_status(ServiceRequest.COMPLETED)
    return redirect('dashboard:index')
