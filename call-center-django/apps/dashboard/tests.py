from django.test import TestCase

from django.contrib.auth.models import User
from .models import Operator
from call_center.models import root_menu, Call, ProblemCategory, Menu, ServiceRequest


class TestOperator(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username='jacob', email='jacob@_2', password='top_secret'
        )
        self.user_other = User.objects.create_user(
            username='jacob_2', email='jacob@_2', password='top_secret'
        )
        self.operator = self.user.operator
        self.operator_other = self.user_other.operator

        self.call_1 = Call(phone_number="+70000000000")
        self.call_1.save()
        self.menu_1 = self.call_1.menu_position
        self.cat_1 = self.menu_1.category

        self.cat_2 = ProblemCategory(name="123123", description="123123")
        self.cat_2.save()
        self.menu_2 = Menu(category=self.cat_2, parent=self.menu_1,
                           button=0, redirects_to_operator=True)
        self.menu_2.save()
        self.call_2 = Call(phone_number="+71111111111", menu_position=self.menu_2)
        self.call_2.save()

        self.sr_1 = ServiceRequest(call_id=self.call_1, category_id=self.cat_1.pk)
        self.sr_1.save()
        self.sr_2 = ServiceRequest(call_id=self.call_2, category_id=self.cat_2.pk)
        self.sr_2.save()

    def test_get_service_requests_empty(self):
        """Test that operator gets an empty list of requests when there are none."""
        self.sr_1.delete()
        self.sr_2.delete()
        self.assertFalse(self.operator.get_service_requests())

    def test_get_service_requests_cant_manage(self):
        """Test that operator gets an empty list of service requests if he can manage."""
        self.operator.categories = []
        self.operator.save()
        self.assertFalse(self.operator.get_service_requests())

    def test_get_service_requests_fresh_simple(self):
        """Test that operator gets fresh service requests that he can manage."""
        self.operator.categories = [self.cat_1]
        self.operator.save()
        l = self.operator.get_service_requests()
        self.assertEquals(len(l), 1)
        self.assertEquals(l[0], self.sr_1)

    def test_if_operator_redirects_call_other_will_get_it(self):
        """Test if operator redirects sr other operator will get it."""
        self.operator_other.categories = [self.cat_1]
        self.operator_other.save()
        self.sr_1.accept_by(self.user)
        self.sr_1.change_status(ServiceRequest.REDIRECTED)
        l = self.operator_other.get_service_requests()
        self.assertEquals(len(l), 1)
        self.assertEquals(l[0], self.sr_1)

    def test_if_operator_accepts_the_call_others_wont_see_it(self):
        """Test that if operator accepts the call other operator wont see it."""
        self.operator_other.categories = [self.cat_1]
        self.operator_other.save()
        self.sr_1.accept_by(self.user)
        l = self.operator_other.get_service_requests()
        self.assertEquals(len(l), 0)
