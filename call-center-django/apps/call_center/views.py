from django.shortcuts import render, reverse, redirect
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import json
import os
from pprint import pprint
from config import celery_app
from django.utils import timezone
from .models import Call, Menu, ServiceRequest
# Create your views here.


@csrf_exempt
def menu_action(request, pk, pressed_button):
    call = Call.objects.get(pk=pk)
    try:
        menu = call.change_menu_position(pressed_button)
    except:
        menu = call.menu_position
    response = HttpResponse()
    response.write(menu.category.solution.read())
    response['Content-Length'] = menu.category.solution.size
    return response


@celery_app.task(bind=False)
def check_last_heartbeat(call_pk):
    call = Call.objects.get(pk=call_pk)
    delta_seconds = (timezone.now() - call.last_heartbeat) / timezone.timedelta(seconds=1)
    if delta_seconds > 20:
        print("{} stale".format(call_pk))
        call.change_status(Call.DEAD_STATUS)
        print("Updated status of {}".format(call_pk))
    else:
        print("{} not stale, adding new task".format(call_pk))
        check_last_heartbeat.apply_async((call_pk,), countdown=10)


def call(request, pk):
    call = Call.objects.get(pk=pk)
    context = {
        "call": call,
        "joined": False,
        "root_audio_url": call.menu_position.category.solution.url,
    }
    return render(request, "call_center/call.html", context=context)


def join(request, pk):
    context = {
        "call": Call.objects.get(pk=pk),
        "joined": True,
        "root_audio_url": None,
    }
    return render(request, "call_center/call.html", context=context)


def heartbeat(request, pk):
    Call.objects.get(pk=pk).update_last_heartbeat(timezone.now())
    return HttpResponse(status=200)


def index(request):
    if request.method == "POST":
        print(request.POST)
        phone_number = request.POST["phone_number"]
        phone_number = "+7" + phone_number
        call = Call(phone_number=phone_number)
        call.save()
        check_last_heartbeat.apply_async((call.pk,), countdown=10)
        return redirect("call_center:call", pk=call.pk)
    else:
        return render(request, "call_center/index.html")
