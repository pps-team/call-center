# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-13 15:31
from __future__ import unicode_literals

from django.db import migrations, models


def set_field_values(apps, schema_editor):
    # use apps.get_model("app_name", "model_name") and set the defualt values
    Call = apps.get_model("call_center", "call")
    for call in Call.objects.all():
        call.last_heartbeat = call.start
        call.save()


class Migration(migrations.Migration):

    dependencies = [
        ('call_center', '0005_auto_20161213_1529'),
    ]

    operations = [
        migrations.RunPython(set_field_values),
        migrations.AlterField(
            model_name='call',
            name='last_heartbeat',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
