# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-13 16:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('call_center', '0006_auto_20161213_1531'),
    ]

    operations = [
        migrations.AlterField(
            model_name='problemcategory',
            name='solution',
            field=models.FileField(default='wavs/greetings.wav', max_length=255, upload_to='wavs'),
        ),
    ]
