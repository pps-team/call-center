from django.test import TestCase
from django.utils.timezone import now
from django.contrib.auth.models import User

from .models import Call, ServiceRequest, root_menu, Menu


class TestCall(TestCase):
    def setUp(self):
        """Helper method for setting up the call."""
        self.call = Call(phone_number="+70000000000")
        self.call.save()

    def test_call_heartbeat(self):
        """Test that updating heartbeat updates the actual field."""
        time = now()
        self.call.update_last_heartbeat(time)
        self.assertEquals(self.call.last_heartbeat, time)

    def test_change_status_works(self):
        """Test that chaning the status of the call reflects in changes of the model's field."""
        self.call.change_status(Call.DEAD_STATUS)
        self.assertEquals(self.call.status, Call.DEAD_STATUS)

    def test_change_status_changes_sr_status(self):
        """Test that changing the status of a call also changes the status of ServiceRequest."""
        root_pk = root_menu()
        root = Menu.objects.get(pk=root_pk)
        sr = ServiceRequest(category=root.category, call_id=self.call)
        sr.save()
        self.call.change_status(Call.DEAD_STATUS)
        sr.refresh_from_db()
        self.assertEquals(sr.status, ServiceRequest.COMPLETED)

    def test_that_changing_menu_position_works(self):
        """Test that changing call's menu position works."""
        root_menu = self.call.menu_position
        child_menu = Menu(parent=root_menu, category=root_menu.category,
                          button=5, redirects_to_operator=False)
        child_menu.save()
        new_position = self.call.change_menu_position(5)
        self.assertEquals(new_position, child_menu)

    def test_that_changing_menu_position_creates_service_request(self):
        """Test that chaning menu position creates sr when menu redirects to opeator."""
        root_menu = self.call.menu_position
        child_menu = Menu(parent=root_menu, category=root_menu.category,
                          button=5, redirects_to_operator=True)
        child_menu.save()
        self.call.change_menu_position(5)
        sr = ServiceRequest.objects.get(call_id=self.call)
        self.assertEquals(sr.status, ServiceRequest.CREATED)


class TestServiceRequest(TestCase):
    def setUp(self):
        self.call = Call(phone_number="+70000000000")
        self.call.save()
        self.category = self.call.menu_position.category
        self.sr = ServiceRequest(category=self.category, call_id=self.call)
        self.user = User.objects.create_user(
            username='jacob', email='jacob@…', password='top_secret'
        )

    def test_str_status_works(self):
        """Test that str_status works as expected for all types of status."""
        self.assertEquals(self.sr.str_status, "Created")
        self.sr.change_status(ServiceRequest.ACCEPTED)
        self.assertEquals(self.sr.str_status, "Accepted by operator")
        self.sr.change_status(ServiceRequest.REDIRECTED)
        self.assertEquals(self.sr.str_status, "Redirected to another operator")
        self.sr.change_status(ServiceRequest.COMPLETED)
        self.assertEquals(self.sr.str_status, "Completed")

    def test_change_status_works(self):
        """Test that change status works as expected."""
        self.sr.change_status(ServiceRequest.ACCEPTED)
        self.assertEquals(self.sr.status, ServiceRequest.ACCEPTED)
        self.sr.change_status(ServiceRequest.REDIRECTED)
        self.assertEquals(self.sr.status, ServiceRequest.REDIRECTED)
        self.sr.change_status(ServiceRequest.COMPLETED)
        self.assertEquals(self.sr.status, ServiceRequest.COMPLETED)
        self.sr.change_status(ServiceRequest.CREATED)
        self.assertEquals(self.sr.status, ServiceRequest.CREATED)

    def test_accepted_works(self):
        """Test accepted changes status and sets operator_id."""
        self.sr.accept_by(self.user)
        self.assertEquals(self.sr.operator_id, self.user)
        self.assertEquals(self.sr.status, ServiceRequest.ACCEPTED)
