from django.db import models
from django.core.validators import RegexValidator
from django.contrib.auth.models import User


class ProblemCategory(models.Model):
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    solution = models.FileField(upload_to='wavs', max_length=255,
                                default='wavs/greetings.wav')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Problem categories"


class Menu(models.Model):
    parent = models.ForeignKey('self', blank=True, null=True)
    category = models.ForeignKey(ProblemCategory)
    button = models.DecimalField(max_digits=1, decimal_places=0)
    redirects_to_operator = models.BooleanField(default=False)

    class Meta:
        unique_together = (('parent', 'button'),)

    def __str__(self):
        return "{0} {1}".format(str(self.pk), self.category.name)


def root_menu():
    category, _ = ProblemCategory.objects.get_or_create(name="Root category",
                                                        description="Description")
    return Menu.objects.get_or_create(category=category, button=0)[0].pk


class Call(models.Model):
    LIVE_STATUS = 1
    DEAD_STATUS = 2
    STATUS_CHOICES = (
        (LIVE_STATUS, 'Live'),
        (DEAD_STATUS, 'Dead'),
    )

    last_heartbeat = models.DateTimeField(auto_now_add=True)
    start = models.DateTimeField(auto_now_add=True)
    end = models.DateTimeField(auto_now=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number should be in format: '+999999999'."
                                 " Up to 15 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex],
                                    max_length=255,
                                    blank=True)

    status = models.PositiveSmallIntegerField(default=LIVE_STATUS,
                                              choices=STATUS_CHOICES)
    menu_position = models.ForeignKey(Menu, default=root_menu)

    def __str__(self):
        return "{0} ({1}) {2}".format(self.phone_number,
                                      self.menu_position,
                                      self.status)

    def change_status(self, new_status):
        self.status = new_status
        self.save()
        if new_status == self.DEAD_STATUS:
            try:
                service_request = ServiceRequest.objects.get(call_id=self)
                service_request.change_status(ServiceRequest.COMPLETED)
            except ServiceRequest.DoesNotExist:
                pass

    def update_last_heartbeat(self, time):
        self.last_heartbeat = time
        self.save()

    def change_menu_position(self, pressed_button):
        new_position = Menu.objects.get(parent=self.menu_position, button=pressed_button)
        # Switch to new step of Menu
        self.menu_position = new_position
        self.save()
        if new_position.redirects_to_operator:
            sr = ServiceRequest.objects.create(category=new_position.category, call_id=self)
            sr.save()
        return new_position


class ServiceRequest(models.Model):
    CREATED = 2
    ACCEPTED = 1
    REDIRECTED = 3
    COMPLETED = 0
    REQUEST_STATUSES = [(CREATED, 'Created'),
                        (ACCEPTED, 'Accepted by operator'),
                        (REDIRECTED, 'Redirected to another operator'),
                        (COMPLETED, 'Completed')]

    created_at = models.DateTimeField(auto_now_add=True)
    completed_at = models.DateTimeField(auto_now=True)
    category = models.ForeignKey(ProblemCategory)

    operator_id = models.ForeignKey(User, blank=True, null=True)
    call_id = models.ForeignKey(Call)
    status = models.IntegerField(choices=REQUEST_STATUSES, default=CREATED)

    def __str__(self):
        return "Category: {} Status: {} Created_at: {}".format(self.category,
                                                               self.str_status,
                                                               self.created_at)

    @property
    def str_status(self):
        for status, str_status in self.REQUEST_STATUSES:
            if status == self.status:
                return str_status

    def change_status(self, new_status):
        self.status = new_status
        self.save()

    def accept_by(self, user):
        self.operator_id = user
        self.status = ServiceRequest.ACCEPTED
        self.save()
