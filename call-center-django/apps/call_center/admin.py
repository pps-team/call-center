from django.contrib import admin
from .models import Call, Menu, ProblemCategory, ServiceRequest


class CallAdmin(admin.ModelAdmin):
    readonly_fields = ("last_heartbeat", "start", "end")


# Register your models here.
admin.site.register(Call, CallAdmin)
admin.site.register(Menu)
admin.site.register(ProblemCategory)
admin.site.register(ServiceRequest)
