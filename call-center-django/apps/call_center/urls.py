"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from .views import menu_action, index, call, join, heartbeat

app_name = 'call_center'
urlpatterns = [
    url(r'call/(?P<pk>\d+)/$', call, name="call"),
    url(r'call/(?P<pk>\d+)/join$', join, name="join"),
    url(r'call/(?P<pk>\d+)/heartbeat$', heartbeat, name="heartbeat"),
    url(r'call/(?P<pk>\d+)/menu/(?P<pressed_button>.+)$', menu_action, name="menu_action"),
    url(r'$', index, name="index"),
]
