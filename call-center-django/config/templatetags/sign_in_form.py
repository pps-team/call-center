"""Tag that creates custom sign in form."""

from django import template
from django.forms import EmailField, BooleanField, CharField
register = template.Library()


@register.inclusion_tag('sign_in.html')
def sign_in_form(form):
    """Retrieve form fields."""
    fields = form.fields.items()
    checkbox = None
    email = None
    password = None
    all_fields = []
    for field in fields:
        if isinstance(field[1], BooleanField):
            checkbox = field[0]
        elif isinstance(field[1], EmailField):
            email = field[0]
        elif isinstance(field[1], CharField):
            password = field[0]
        all_fields.append(field[1])
    email_errors = []
    password_errors = []
    checkbox_errors = []
    for field, field_errors in form.errors.items():
        if field == email:
            email_errors = field_errors
        elif field == password:
            password_errors = field_errors
        elif field == checkbox:
            checkbox_errors = field_errors

    return {'email': email, 'checkbox': checkbox, 'password': password,
            'email_errors': email_errors, 'password_errors': password_errors,
            'checkbox_errors': checkbox_errors, 'form': form}
