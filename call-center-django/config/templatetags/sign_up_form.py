"""Tag that creates custom sign up form."""

from django import template
from django.forms import EmailField, CharField
register = template.Library()


@register.inclusion_tag('sign_up.html')
def sign_up_form(form):
    """Retrieve form fields."""
    fields = form.fields.items()
    email = None
    password = None
    password2 = None
    all_fields = []
    hit = False
    for field in fields:
        if isinstance(field[1], EmailField):
            email = field[0]
        elif isinstance(field[1], CharField):
            if hit:
                password2 = field[0]
                break
            else:
                password = field[0]
                hit = True
        all_fields.append(field[1])
    email_errors = []
    password_errors = []
    password2_errors = []
    for field, field_errors in form.errors.items():
        if field == email:
            email_errors = field_errors
        elif field == password:
            password_errors = field_errors
        elif field == password2:
            password2_errors = field_errors
    return {'email': email, 'password': password, 'password2': password2,
            'email_errors': email_errors, 'password_errors': password_errors,
            'second_password_errors': password2_errors, 'form': form}
