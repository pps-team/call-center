"""Views module."""
from django.views.generic import TemplateView


class ExtraContentTemplateView(TemplateView):
    """TemplateView which accepts extra parameters."""

    extra_content = None

    def get_context_data(self, *args, **kwargs):
        """get_context_data that adds extra_content to TemplateView."""
        context = super().get_context_data(*args, **kwargs)
        if self.extra_content:
            context.update(self.extra_content)
        return context
