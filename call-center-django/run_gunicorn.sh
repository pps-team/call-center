#! /bin/sh
gunicorn \
    --bind="0.0.0.0:8000"\
    --workers=${NUM_GUNICORN_WORKERS}\
    --forwarded-allow-ips="*"\
    config.wsgi
