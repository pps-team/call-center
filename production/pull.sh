git pull
docker-compose build
docker-compose pull
docker-compose scale web=0 worker=0 node_signaling=0
docker-compose scale node_signaling=1 worker=1 web=1
docker-compose restart nginx
docker-compose run --rm web ./manage.py migrate
docker-compose run --rm web ./manage.py collectstatic --clear --noinput
